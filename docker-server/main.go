package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

// Handles HTTP requests to the root path "/".
func getRoot(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got / request\n")
	io.WriteString(w, "This is my website!\n")
}

// Handles HTTP requests to the path "hello/".
func getHello(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("got /hello request\n")
	io.WriteString(w, "Hello, HTTP!\n")
}

func main() {
	// The server multiplexer is a http.Handler that calls the handler func associated with a given path.
	// Sets up handler function for root path in default server multiplexer.
	http.HandleFunc("/", getRoot)
	// Sets up handler function for "hello/" path in default server multiplexer.
	http.HandleFunc("/hello", getHello)

	// Tells the global HTTP server to listen for incoming requests on a specific port with an optional handler.
	// Network port :3333 used without IP address to enable multi-program communication.
	// nil handler argument tells ListenAndServe to use default server multiplexer.
	err := http.ListenAndServe("0.0.0.0:3333", nil)
	if err != nil {
		fmt.Printf("Error starting server %s\n", err)
		os.Exit(1)
	}
}
